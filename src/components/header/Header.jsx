import React from 'react';
import {Avatar, Button, Popover, Space, Tooltip} from "antd";
import { PlusOutlined , LogoutOutlined,OrderedListOutlined} from '@ant-design/icons';
import {logout} from "../../redux/reducers/auth-reduser";

import {NavLink} from "react-router-dom";

export default function Header(props) {

    return (
            <nav className="nav">
                <div>
                    <Popover placement="bottom" title="User"
                             content={
                                 <div>
                                     <h5>Username : {props.user.username}</h5>
                                     <h5>Email : {props.user.email}</h5>
                                 </div>
                             }
                             trigger="hover">
                        <Avatar size="large" style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>
                            {props.user.username.charAt(0)}
                        </Avatar>
                    </Popover>
                </div>
                <div>
                    <Space size="large">
                        <NavLink to="/workspace/create">
                            <Button type="primary" icon={<PlusOutlined />} size="large">
                                Add Workspace
                            </Button>
                        </NavLink>
                        <Tooltip placement="bottom" title={"Workspace List"}>
                            <NavLink to="/">
                                <Button type="primary" icon={<OrderedListOutlined />} size="large" />
                            </NavLink>
                        </Tooltip>
                        <Tooltip placement="bottom" title={"Logout"}>
                            <Button onClick={logout} type="primary" shape="circle" icon={<LogoutOutlined />} size="large" />
                        </Tooltip>
                    </Space>
                </div>
            </nav>
    );
}