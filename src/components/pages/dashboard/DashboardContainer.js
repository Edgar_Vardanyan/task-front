import React from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import Dashboard from "./Dashboard";
import {getUserData} from "../../../redux/selectors/user-selectors";

const DashboardContainer  = (props)=>{

    return(<Dashboard user={props.user} />)
};

const mapStateToProps = (state) => {
    return {
        user: getUserData(state)
    }
};

const mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(DashboardContainer);


