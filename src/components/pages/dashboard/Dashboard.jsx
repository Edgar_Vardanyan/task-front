import React from 'react';
import  './dashboard.scss'
import PrivateRoute from "../../../HOC/PrivateRoute";
import {Switch} from "react-router";
import WorkspaceContainer from "../workspace/WorkspaceContainer";
import CreateContainer from "../workspace/create/CreateContainer";
import EditContainer from "../workspace/edit/EditContainer";
import Header from "../../header/Header";

export default function Dashboard(props) {
    return (
            <div className="dashboard-wrapper">
                <Header user={props.user}/>
                <div className="body">
                    <Switch>
                        <PrivateRoute exact path="/workspace/create" auth={true} component={CreateContainer} />
                        <PrivateRoute exact path="/workspace/edit/:id" auth={true} component={EditContainer} />
                        <PrivateRoute exact path="/" auth={true} component={WorkspaceContainer} />
                    </Switch>
                </div>
            </div>
    );
}