import React, {useState} from 'react';
import {NavLink} from "react-router-dom";
import {Button, Modal, Space} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";
import DataTableContainer from "../../dataTable/DataTableContainer";

export default function Workspace(props) {

    const [deleteConfirm, setDeleteConfirm] = useState({
        visible: false,
        value: null
    });

    const action = (row) => {
        return (
            <Space size="middle">
                <NavLink to={"/workspace/edit/"+row._id}>
                    <Button
                        type="default"
                        icon={<EditOutlined />}
                    />
                </NavLink>
                <Button
                    type="danger"
                    icon={<DeleteOutlined />}
                    onClick={() => setDeleteConfirm({visible: true,value: row._id})}
                    loading={false}
                />
            </Space>
        )
    }

    const onDeleteConfirm = () =>{
        props.handleRemove(deleteConfirm.value)()
        setDeleteConfirm({visible: false , value: null});
    }

    return (
        <div>
            <Modal
                title="Delete Workspace."
                centered
                visible={deleteConfirm.visible}
                onOk={onDeleteConfirm}
                onCancel={() => setDeleteConfirm({visible: false , value: null})}
            >
                <p>Are you sure?</p>
            </Modal>
            <DataTableContainer data={props.data}
                                action={action}
            />
        </div>
    );
}