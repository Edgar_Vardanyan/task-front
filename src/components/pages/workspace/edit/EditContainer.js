import React, {useEffect, useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import WorkspaceForm from "../form/WorkspaceForm";
import {useHistory} from "react-router";
import {WorkspaceAPI} from "../../../../Api/api";
import {useParams} from "react-router";
import {openNotificationWithIcon} from "../../../../Helpers/notification";

const EditContainer  = (props) => {
    const history = useHistory();
    const {id} = useParams();
    // const
    const [loading,setLoading] = useState(false);
    const [formErrors ,setFormErrors] = useState({});
    const [formData ,setFormData] = useState({});

    useEffect(()=>{
        if (id){
            setLoading(true);
            WorkspaceAPI.workspace(id).then((res)=>{
                if (res.success){
                    setFormData(res.data)
                }
                setLoading(false);
            })
        }else{
            history.push('/')
        }
    },[history, id])

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);

        WorkspaceAPI.edit({...formData,id}).then((res)=>{
            if (res.success){
                openNotificationWithIcon({type:'success',title: "Message" ,text: res.message })
                history.push("/")
                return;
            }else{
                setFormErrors(res.errors)
            }
            setLoading(false);
        })
    }

    const handleChange = e => {
        let currentElement = e.target;
        const { name , value } = currentElement;
        setFormData({ ...formData, [name] : value });
    };

    const switcherChange = (value)=>{
        setFormData((currentState)=>({...currentState,type: value ? 'Public' : 'Private'}))
    }

    return(
        <>
            { Object.entries(formData).length > 0 && <WorkspaceForm
                mode="edit"
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                formData={formData}
                formErrors={formErrors}
                loading={loading}
                switcherChange={switcherChange}
                />
            }
        </>)
};

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(EditContainer);


