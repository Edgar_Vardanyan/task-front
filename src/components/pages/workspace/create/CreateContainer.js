import React, {useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import WorkspaceForm from "../form/WorkspaceForm";
import {useHistory} from "react-router";
import {WorkspaceAPI} from "../../../../Api/api";
import {openNotificationWithIcon} from "../../../../Helpers/notification";


const CreateContainer  = (props) => {
    const history = useHistory();

    const [loading,setLoading] = useState(false);
    const [formErrors ,setFormErrors] = useState({});
    const [formData ,setFormData] = useState({
        name: '',
        description: '',
        type: 'Public',
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        WorkspaceAPI.create(formData).then((res)=>{
            if (res.success){
                openNotificationWithIcon({type:'success',title: "Message" ,text: res.message })
                history.push("/")
                return;
            }else{
                setFormErrors(res.errors)
            }
            setLoading(false);
        })
    }

    const handleChange = e => {
        let currentElement = e.target;
        const { name , value } = currentElement;
        setFormData({ ...formData, [name] : value });
    };

    const switcherChange = (value)=>{
        setFormData((currentState)=>({...currentState,type: value ? 'Public' : 'Private'}))

    }

    return(<WorkspaceForm
            mode="create"
            handleSubmit={handleSubmit}
            handleChange={handleChange}
            formData={formData}
            formErrors={formErrors}
            loading={loading}
            switcherChange={switcherChange}
    />)
};

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(CreateContainer);


