import React from 'react';
import {Button, Input, Space, Switch} from "antd";
import {SaveOutlined,EditOutlined, UserOutlined,RollbackOutlined} from "@ant-design/icons";
import {NavLink} from "react-router-dom";


export default function WorkspaceForm(props) {

    const {handleChange,handleSubmit,formData,formErrors,loading,mode} = props;

    return (
        <div>
            <form noValidate onSubmit={handleSubmit}  autoComplete="nope" >
                <p className="error-block">{formErrors.form}</p>
                <div className="form-control">
                    <Input size="large"
                           type="text"
                           name="name"
                           placeholder="Name"
                           value={formData.name}
                           onChange={handleChange}
                           prefix={<UserOutlined />}
                    />
                    <p className="error-block">{formErrors.name}</p>
                </div>
                <div className="form-control">
                    <Input.TextArea
                           size="large"
                           name="description"
                           placeholder="Description"
                            value={formData.description}
                            onChange={handleChange}
                    />
                    <p className="error-block">{formErrors.description}</p>
                </div>
                <div className="form-control" >
                    <Space>
                        <span>Make private</span>
                        <Switch
                            defaultChecked={formData.type === 'Public'}
                            onChange={props.switcherChange}
                            name="type"
                        />
                    </Space>
                </div>
                <div className="btn-wrapper">
                    <div className="content-right">
                        <Space>
                            <NavLink to="/">
                                <Button
                                    type="default"
                                    icon={<RollbackOutlined />}
                                >Cancel</Button>
                            </NavLink>
                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={mode === 'create' ? <SaveOutlined /> : <EditOutlined/>}
                                loading={loading}
                            >Save</Button>
                        </Space>
                    </div>
                </div>
            </form>
        </div>
    );
}