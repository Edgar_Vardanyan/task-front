import React, {useEffect, useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import Workspace from "./Workspace";
import {WorkspaceAPI} from "../../../Api/api";
import {openNotificationWithIcon} from "../../../Helpers/notification";


const WorkspaceContainer  = (props) => {
    const [data,setData] = useState();
    const [render,setRender] = useState(false)
    useEffect(()=>{
        WorkspaceAPI.list().then((res)=>{
            if (res.success){
                setData(res.data)
            }
        })
    },[render])


    const handleRemove =(id) => () => {
        WorkspaceAPI.delete(id).then((res)=>{
            let type = 'error'
            if (res.success){
                type = 'success'
                setRender((currState)=>(!currState))
            }
            openNotificationWithIcon({type,title: "Message" ,text: res.message })

        })
    }

    return(<Workspace
        handleRemove={handleRemove}
        data={data}
    />)
};

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(WorkspaceContainer);


