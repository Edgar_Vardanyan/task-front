import React, {useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {Register} from "./Register.jsx";
import {useHistory} from "react-router";
import {AuthAPI} from "../../../../Api/api";
import {openNotificationWithIcon} from "../../../../Helpers/notification";

let RegisterContainer = (props) =>{
    const history = useHistory();

    let initialFormData = {
        username: null,
        email: null,
        password: null,
        passwordConfirmation: null,
    };

    const [loading,setLoading] = useState(false)
    const [formData ,setFormData] = useState(initialFormData);
    const [formsErrors , setFormsErrors] = useState({});



    let handleSubmit =(e)=>{
        e.preventDefault();
        setLoading(true)
        AuthAPI.register(formData).then( data =>{
            if (data.success === false){
                setFormsErrors(data.errors);
            }else{
                openNotificationWithIcon({type:'success',title: "Registration" ,text: data.message })
                history.push('/login')
                return;
            }
            setLoading(false);
        })
    };

    let handleChange = (e)=>{
        let currentElement = e.target;
        const { name , value } = currentElement;
        setFormData({ ...formData, [name] : value });
    };

    return(
        <Register
            handleSubmit={handleSubmit}
            handleChange={handleChange}
            formData = {formData}
            errorMessages={formsErrors}
            loading={loading}
        />
    )

}

let mapStateToProps = (state) => {
    return {
    }
};
let mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(RegisterContainer);