import React from "react";
import {
    UserOutlined,
    MailOutlined,
    LockOutlined,
    EyeTwoTone,
    EyeInvisibleOutlined,
    UserAddOutlined
} from "@ant-design/icons";
import {Button, Input} from "antd";


export const Register = (props)=> {

    const {handleChange,handleSubmit,formData,errorMessages,loading} = props;

    return(
        <div>
            <div className="auth_title_box">
                <strong>Create an account</strong>
                <span>Fill in all the fields</span>
            </div>
            <form  noValidate onSubmit={handleSubmit}  autoComplete="nope">
                <p className="error-block">{errorMessages.form}</p>
                <div className="form-control">
                    <Input size="large"
                           type="text"
                           name="username"
                           placeholder="Username"
                           value={formData.username}
                           onChange={handleChange}
                           prefix={<UserOutlined />}
                    />
                    <p className="error-block">{errorMessages.username}</p>
                </div>
                <div className="form-control">
                    <Input size="large"
                           type="text"
                           name="email"
                           placeholder="Email"
                           value={formData.email}
                           onChange={handleChange}
                           prefix={<MailOutlined />}
                    />
                    <p className="error-block">{errorMessages.email}</p>
                </div>
                <div className="form-control">
                    <Input.Password
                        size="large"
                        name="password"
                        placeholder="Password"
                        value={formData.password}
                        onChange={handleChange}
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        prefix={<LockOutlined />}
                    />
                    <p className="error-block">{errorMessages.password}</p>
                </div>
                <div className="form-control">
                    <Input.Password
                        size="large"
                        name="passwordConfirmation"
                        placeholder="Confirm Password"
                        value={formData.password_confirmation}
                        onChange={handleChange}
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        prefix={<LockOutlined />}
                    />
                    <p className="error-block">{errorMessages.passwordConfirmation}</p>
                </div>
                <div className="btn-wrapper">
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={<UserAddOutlined />}
                        loading={loading}
                        block
                    >Register</Button>
                </div>
            </form>
        </div>
    )
};



