import React from "react";
import {Route} from "react-router";
import { NavLink} from "react-router-dom";
import "./auth.scss";
import LoginContainer from "./login/LoginContainer";
import RegisterContainer from "./register/RegisterContainer";

const AuthPage = (props) => {

    return (
            <div className="auth_container" >

                <div className="page_switcher">
                    <div className="link_wrapper">
                        <NavLink exact   to="/login">
                            Login
                        </NavLink>
                    </div>
                    <div className="link_wrapper">
                        <NavLink exact  to="/register">
                            Register
                        </NavLink>
                    </div>
                    <div className={"switcher-btn" + (props.path === "/register" ? " active_first" : " active_second")}> </div>
                </div>

                <div className="auth_content">
                    <Route path="/login" render={()=><LoginContainer/>}/>
                    <Route path="/register" render={()=><RegisterContainer/>}/>
                </div>
            </div>
    )
};


export default AuthPage;