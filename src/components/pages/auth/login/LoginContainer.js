import React, {useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import Login from "./Login.jsx";
import {AuthAPI} from "../../../../Api/api";

const LoginContainer  = (props)=>{

    const [loading,setLoading] = useState(false);
    const [formErrors ,setFormErrors] = useState({});
    const [formData ,setFormData] = useState({
        username: null,
        password: null,
    });


    const handleSubmit = e => {
        e.preventDefault();
        setLoading(true)
        AuthAPI.login(formData).then( data => {
            if ( data.success === true ){
                localStorage.setItem('token', data.user.token);
                window.location.href = '/';
            }else{
                setFormErrors(data.errors)
            }
            setLoading(false)
        });
    };

    const handleChange = e => {
        let currentElement = e.target;
        const { name , value } = currentElement;
        setFormData({ ...formData, [name] : value });
    };

    return(<Login
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                formData = {formData}
                errorMessages={formErrors}
                loading={loading}
        />)

};

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps =  {
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(LoginContainer);


