import React from 'react';
import {Button, Input} from "antd";
import { UserOutlined ,LockOutlined,EyeTwoTone,EyeInvisibleOutlined,LoginOutlined} from '@ant-design/icons';

export default function Login(props) {

    const {handleChange,handleSubmit,formData,errorMessages,loading} = props;

    return (
            <React.Fragment>
                <div className="auth_title_box">
                    <strong>Welcome back!</strong>
                    <span>Enter your username and password</span>
                </div>
                <form  noValidate onSubmit={handleSubmit}  autoComplete="nope">
                    <p className="error-block">{errorMessages.form}</p>
                    <div className="form-control">
                        <Input size="large"
                               type="text"
                               name="username"
                               placeholder="Username"
                               value={formData.username}
                               onChange={handleChange}
                               prefix={<UserOutlined />}
                        />
                        <p className="error-block">{errorMessages.username}</p>
                    </div>
                    <div className="form-control">
                        <Input.Password
                            size="large"
                            name="password"
                            placeholder="Password"
                            value={formData.password}
                            onChange={handleChange}
                            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            prefix={<LockOutlined />}
                        />
                        <p className="error-block">{errorMessages.password}</p>
                    </div>
                    <div className="btn-wrapper">
                        <Button
                            type="primary"
                            htmlType="submit"
                            icon={<LoginOutlined />}
                            loading={loading}
                            block
                        >Login</Button>
                    </div>
                </form>

            </React.Fragment>
    );
}