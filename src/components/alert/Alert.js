import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {connect} from "react-redux";
import {isAuthStatus} from "../redux/selectors/user-selectors";

/**
 * example <PrivateRoute exact path="/" component={Component} />
 * @param Component
 * @param isAuth
 * @param auth
 * @param rest
 * @returns {*}
 * @constructor
 */
const PrivateRoute = (props) => {
    return(
        <div></div>
    )
};

let mapStateToProps = (state) => {
    return {

    }
};

let mapDispatchToProps =  {
};

export default connect(mapStateToProps,mapDispatchToProps)(PrivateRoute);