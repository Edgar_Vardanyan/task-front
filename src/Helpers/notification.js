import {notification} from "antd";

export const openNotificationWithIcon = (data) => {
    notification[data.type]({
        message: data.title,
        description: data.text,
        placement: 'bottomRight'
    });
};