import authHeader from "./authHeader";

let baseUrl = 'http://localhost:8080/api' ;

export const AuthAPI = {
    register(body){
        return fetch(baseUrl+`/auth/signup`,authHeader('POST',body))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },
    login(body){
        return fetch(baseUrl+`/auth/signin`,authHeader('POST',body))
            .then(response => response.json())
            .then(response => response)
    },
    getAuthUser(){
        return fetch(baseUrl+`/auth/user`,authHeader('GET'))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },

};

export const WorkspaceAPI = {
    create(body){
        return fetch(baseUrl+`/workspace/create`,authHeader('POST',body))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },
    edit(body){
        return fetch(baseUrl+`/workspace/edit`,authHeader('PUT',body))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },
    delete(id){
        return fetch(baseUrl+`/workspace/delete/${id}`,authHeader('DELETE'))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },
    list(){
        return fetch(baseUrl+`/workspace/list`,authHeader('GET'))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },
    workspace(id){
        return fetch(baseUrl+`/workspace/${id}`,authHeader('GET'))
            .then(response => response.json())
            .then(response => {
                return response
            })
    },


};


