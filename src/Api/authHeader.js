/**
 *
 * @param method
 * @param body
 * @returns {{headers: {Authorization: string, "Content-Type": string}, method: *, body: string}|{headers: {"Content-Type": string}, method: *, body: string}}
 */
import {getToken} from "../redux/reducers/auth-reduser";
const authHeader = (method, body={}) =>{
    // return authorization header with jwt token
    let token = getToken();

    if (['POST','PUT'].includes(method)){
        if (token) {
            return {
                method: method,
                headers :{
                    'Authorization': 'Bearer ' +token,
                    'Content-Type' : 'Application/json',
                },
                body: JSON.stringify(body)
            };
        } else {
            return {
                method: method,
                headers :{
                    'Content-Type' : 'Application/json',
                },
                body: JSON.stringify(body)
            };
        }
    }else{
        if (token){
            return {
                method: method,
                headers :{
                    'Content-Type' : 'Application/json',
                    'Authorization': 'Bearer ' +token,
                },
            };
        }else{
            return {
                method: method,
                headers :{
                    'Content-Type' : 'Application/json',
                },
            };
        }

    }

};

export const onlyToken = ( method , body)=>{
    let token = getToken();
    return {
        method: method,
        headers :{
            'Authorization': 'Bearer ' +token,
        },
        body: body
    };
};
export default authHeader;