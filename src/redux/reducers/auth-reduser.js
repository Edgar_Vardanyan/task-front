import {AuthAPI} from "../../Api/api";

const SET_USER_DATA = "SET_USER_DATA";

let initialStore = {

    //user data
    user:  null,
    isAuth: false,

};

const AuthReducer = (state = initialStore, action ) => {
    switch (action.type){
        case SET_USER_DATA:
            return {
                ...state,
                user: action.data,
                isAuth: true
            };
        default:
         return   state
    }
};
/**                     Action Creators
 * **************************************************
 * @param userData
 * @returns {{data: {login: *, email: *}, type: string}}
 */
export const setAuthUserData = (userData) => ({
    type: SET_USER_DATA,
    data: userData,
});


/**                     Thank
 * *****************************************************
 * @param setInit
 * @returns {(function(*): void)|*}
 */
export const getAuthUserThank = (setInit) => {
    return (dispatch) => {
        AuthAPI.getAuthUser().then(function (response) {
            if (response.success) {
                dispatch(setAuthUserData(response.user));
            }
        }).finally(()=>{
            setInit(true)
        });
    }
};




/**
 * remove user from local storage and store to log user out
 * @returns {function(...[*]=)}
 */
export const logout = () => {
    localStorage.removeItem('token');
    window.location.href = '/';
};


/************************************Auth Service***********************************************/
/**
 * @returns {string}
 */
export const getToken = ()=>{
    const token = localStorage.getItem('token');
    if (token && token.length > 0 && token !=='undefined' && typeof token === 'string'){
        return token;
    }
    return null ;
};


export default AuthReducer;