
/**
 * @param state
 * @returns {string | number | null | }
 */
export const getUserData = (state)=>{
    return state.auth.user;
};
/**
 * @param state
 * @returns {boolean}
 */
export const isAuthStatus = (state)=>{
    return state.auth.isAuth;
};

