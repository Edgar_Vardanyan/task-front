import App from "./App";
import {compose} from "redux";
import {connect} from "react-redux";
import React from "react";
import {getAuthUserThank} from "./redux/reducers/auth-reduser";
import {isAuthStatus} from "./redux/selectors/user-selectors";


class AppContainer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            init: false
        }
        this.init = this.init.bind(this);
    }

    init(value){
        this.setState({init:value})
    }

    componentDidMount() {
        this.props.getAuthUserThank(this.init)
    }

    render() {
        return(
            <React.Fragment>
                { this.state.init && <App/>}
            </React.Fragment>
        )
    }
}


let mapStateToProps = (state) => {
    return {
        isAuth: isAuthStatus(state),
    }
};
let mapDispatchToProps =  {
    getAuthUserThank
};

export default compose(
    connect(mapStateToProps,mapDispatchToProps)
)(AppContainer);
