import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {connect} from "react-redux";
import {isAuthStatus} from "../redux/selectors/user-selectors";

/**
 * example <PrivateRoute exact path="/" component={Component} />
 * @param Component
 * @param isAuth
 * @param auth
 * @param rest
 * @returns {*}
 * @constructor
 */
const PrivateRoute = ({ component: Component , auth ,isAuth , ...rest }) => {
    return(
        <Route {...rest} render={props => {
            rest.history = props.history;

            let redirectPath = '/';
            if(auth === true && !isAuth){
                redirectPath = '/login'
            }
            return( isAuth === auth
                ? <Component {...rest} />
                : <Redirect to={{pathname: redirectPath, state: {from: props.location}}}/>)
        }} />
    )
};

let mapStateToProps = (state) => {
    return {
        isAuth : isAuthStatus(state),
    }
};

let mapDispatchToProps =  {
};

export default connect(mapStateToProps,mapDispatchToProps)(PrivateRoute);