import './App.scss';
import {Switch} from "react-router";
import PrivateRoute from "./HOC/PrivateRoute";
import AuthPage from "./components/pages/auth/AuthPage";
import DashboardContainer from "./components/pages/dashboard/DashboardContainer";
import React from "react";

function App() {

  return (
    <div className="App">
        <Switch>
          <PrivateRoute exact path="/login" auth={false} component={AuthPage} />
          <PrivateRoute exact path="/register" auth={false} component={AuthPage} />
          <PrivateRoute  path="/" auth={true} component={DashboardContainer} />
      </Switch>
    </div>
  );
}

export default App;
